import { s__, __ } from '~/locale';

export const I18N_DASHBOARD_LIST_TITLE = s__('Analytics|Analytics dashboards');
export const I18N_DASHBOARD_LIST_DESCRIPTION = s__(
  'Analytics|Dashboards are created by editing the projects dashboard files.',
);
export const I18N_DASHBOARD_LIST_LEARN_MORE = __('Learn more.');
export const I18N_DASHBOARD_LIST_INSTRUMENTATION_DETAILS = s__(
  'ProductAnalytics|Instrumentation details',
);
export const I18N_DASHBOARD_LIST_SDK_HOST = s__('ProductAnalytics|SDK Host');
export const I18N_DASHBOARD_LIST_SDK_DESCRIPTION = s__(
  'ProductAnalytics|The host to send all tracking events to',
);
export const I18N_DASHBOARD_LIST_SDK_APP_ID = s__('ProductAnalytics|SDK App ID');
export const I18N_DASHBOARD_LIST_SDK_APP_ID_DESCRIPTION = s__(
  'ProductAnalytics|Identifies the sender of tracking events',
);
